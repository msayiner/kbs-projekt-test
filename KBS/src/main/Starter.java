package main;

import java.util.Collection;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import controller.Controller;
import json.JsonFunctions;
import jsonClasses.Community;
import jsonClasses.Item;
import restFunctions.RESTAdmin;
import restFunctions.RESTBitstreams;
import restFunctions.RESTCollections;
import restFunctions.RESTCommunities;
import restFunctions.RESTItems;
import restFunctions.Server;


/**
 * starter 
 *
 */

public class Starter {

	public static void main(String[] args) {


		
		Server server = new Server("192.168.56.101", 8080);


		
		// Login with username and password
		System.out.println("___LOGIN___");
	//	String token = RESTAdmin.login(server, "paul@kloss.info", "dspace");
		String token = RESTAdmin.login(server, "fliesske@gmail.com", "dspace");


/* 
		// COMMUNITIES - GET
		System.out.println("___Alle Communities___");
		RESTCommunities.showAllCommunities(server, token);

		System.out.println("___Alle Top-Level Communities___");
		RESTCommunities.showAllTopCommunities(server, token);

		System.out.println("___Community 1___");
		RESTCommunities.showCommunityWithId(server, token, 1);

		System.out.println("___Collection in Community 1___");
		RESTCommunities.showCollectionInCommunity(server, token, 1);

		System.out.println("___Subcommunities aus Community 1___");
		RESTCommunities.showSubcommunitiesFromCommunityWithId(server, token, 1);

		// COLLECTIONS - GET
		System.out.println("___Alle Collections___");
		RESTCollections.showAllCollections(server, token);

		System.out.println("___Collection 1___");
		RESTCollections.showCollectionWithId(server, token, 1);

		System.out.println("___Items in Collection 1___");
		RESTCollections.showItemsInCollection(server, token, 1);

		// ITEMS - GET
		System.out.println("___Alle Items___");
		RESTItems.showAllItems(server, token);

		System.out.println("___Item 1___");
		RESTItems.showItemWithId(server, token, 1);

		System.out.println("___Metadaten von Item 1___");
		RESTItems.showMetadataFromItemWithId(server, token, 1);

		System.out.println("___Bitstreams von Item 1___");
		RESTItems.showBitstreamFromItemWithId(server, token, 1);

		// BITSTREAMS - GET
		System.out.println("___Alle Bitstreams___");
		RESTBitstreams.showAllBitstreams(server, token);

		System.out.println("___Bitstream 9___");
		RESTBitstreams.showBitstreamWithId(server, token, 9);

		System.out.println("___Policy von Bitstream 9___");
		RESTBitstreams.showPolicyFromBitstreamWithId(server, token, 9);

		System.out.println("___Content von Bitstream 19___");
		RESTBitstreams.getContentFromBitstreamWithId(server, token, 19);
*/
		// Json String in Json-Objekt parsen (Communities)
//		JsonFunctions.parseStringArrayIntoJson(
//				RESTCommunities.showAllCommunities(server, token),
//				Community[].class);
//		 JsonFunctions.parseStringIntoJson(RESTCommunities.showCommunityWithId(server, token, 1), Community.class);

		// Collections in JSON Format umwandeln
//		JsonFunctions.parseStringArrayIntoJson(
//				RESTCollections.showAllCollections(server, token),
//				Collection[].class);
		
		// Items in JSON Format umwandeln
//		JsonFunctions.parseStringArrayIntoJson(
//				RESTItems.showAllItems(server, token),
//				Item[].class);
		
		// Login GUI
//		new Login().loginDlg();
		
		// Top-Level-Community erstellen
//		RESTCommunities.createTopLevelCommunity(server, token, "testCommunity4", "by Paul", "lala", "geht vllt", "sidebarText");
		
		
	
		
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Controller(server, token);

            }
        });
        
        
     // Logout
//		System.out.println("___LOGOUT___");
//		boolean geht = RESTAdmin.logout(server, token);
//		System.out.println(geht);
	}

}
