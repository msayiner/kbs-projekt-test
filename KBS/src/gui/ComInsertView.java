package gui;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controller.Controller;
import restFunctions.RESTCommunities;
import restFunctions.Server;


/**
 * View zum Erstellen einer Top-level-Community
 * 
 * @param server Server
 * @param token Token
 * @param name Community Name
 * @param copyrightText Copyright Text
 * @param introductoryText Introductory Text
 * @param shortDescription Short Description
 * @param sidebarText Sidebar Text
 */

public class ComInsertView extends JDialog implements ActionListener{
	
			JTextField txtFieldName;
			JTextField txtFieldShortDes;
			JTextArea txtAreaCopyright;
			JTextArea txtAreaIntroText;
			JTextArea txtAreaSidebar;
			
			public ComInsertView(ContentPanel ctnPanel, Server server, String token) {
			
			setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
			setTitle("Neue Top-Community anlegen");
//			setBounds(400, 300, 300, 200);
			GridBagLayout gbl = new GridBagLayout();
			setLayout(gbl);
					
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.fill=GridBagConstraints.HORIZONTAL;
			gbc.insets = new Insets(5, 5, 5, 5);
			
			gbc.gridx = 1;
			gbc.gridy = 0;
			gbc.gridwidth = 1;
			txtFieldName = new JTextField(20);
			gbl.setConstraints(txtFieldName, gbc);
			add(txtFieldName);
			
			gbc.gridx = 1;
			gbc.gridy = 1;
			gbc.gridwidth = 1;
			txtAreaCopyright = new JTextArea(10,20);
			gbl.setConstraints(txtAreaCopyright, gbc);
			add(txtAreaCopyright);
			
			gbc.gridx = 1;
			gbc.gridy = 2;
			gbc.gridwidth = 1;
			txtAreaIntroText = new JTextArea(10,20);
			gbl.setConstraints(txtAreaIntroText, gbc);
			add(txtAreaIntroText);
			
			gbc.gridx = 1;
			gbc.gridy = 3;
			gbc.gridwidth = 1;
			txtFieldShortDes = new JTextField(20);
			gbl.setConstraints(txtFieldShortDes, gbc);
			add(txtFieldShortDes);
			
			gbc.gridx = 1;
			gbc.gridy = 4;
			gbc.gridwidth = 1;
			txtAreaSidebar = new JTextArea(10,20);
			gbl.setConstraints(txtAreaSidebar, gbc);
			add(txtAreaSidebar);
			
//			gbc.gridx = 1;
//			gbc.gridy = 0;
//			gbc.gridwidth = 1;
//			box = new JComboBox(search);
//			gbl.setConstraints(box, gbc);
//			add(box);
//			box.addItemListener(new ItemListener(){
//
//				@Override
//				public void itemStateChanged(ItemEvent ie) {	
//					if(box.getSelectedIndex() == 0){
//	                    txtField.setEditable(false);
//	                    txtField.setText("Disable");
//				} if(box.getSelectedIndex() == 1){
//	                    txtField.setEditable(false);
//	                    txtField.setText("Disabled");
//				}else{txtField.setEditable(true);
//				txtField.setText("Enabled");
//				}
//			}});
			
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.gridwidth = 1;
			JLabel lblName = new JLabel("Name:");
			gbl.setConstraints(lblName, gbc);
			add(lblName);
			
			gbc.gridx = 0;
			gbc.gridy = 1;
			gbc.gridwidth = 1;
			JLabel lblCopyright = new JLabel("Copyright Text:");
			gbl.setConstraints(lblCopyright, gbc);
			add(lblCopyright);
			
			gbc.gridx = 0;
			gbc.gridy = 2;
			gbc.gridwidth = 1;
			JLabel lblIntroText = new JLabel("Introductory Text:");
			gbl.setConstraints(lblIntroText, gbc);
			add(lblIntroText);
			
			gbc.gridx = 0;
			gbc.gridy = 3;
			gbc.gridwidth = 1;
			JLabel lblShortDes = new JLabel("Short Description:");
			gbl.setConstraints(lblShortDes, gbc);
			add(lblShortDes);
			
			gbc.gridx = 0;
			gbc.gridy = 4;
			gbc.gridwidth = 1;
			JLabel lblSidebar = new JLabel("Sidebar Text:");
			gbl.setConstraints(lblSidebar, gbc);
			add(lblSidebar);
					

			
			
			
			gbc.gridx = 0;
			gbc.gridy = 5;
			gbc.gridwidth = 1;
			JButton btnSave = new JButton("Speichern");
			gbl.setConstraints(btnSave, gbc);
			add(btnSave);
			btnSave.addActionListener(new ActionListener(){
				
				@Override
				public void actionPerformed(ActionEvent eve){										

					final int option = JOptionPane.showConfirmDialog(txtAreaSidebar, "Top-LevelCommunity speichern?", "Speichern?", JOptionPane.OK_CANCEL_OPTION );
					if(option == JOptionPane.OK_OPTION){
						RESTCommunities.createTopLevelCommunity(server, token, txtFieldName.getText(), txtAreaCopyright.getText(), txtAreaIntroText.getText(), txtFieldShortDes.getText(), txtAreaSidebar.getText());
						System.out.println(token);
						System.out.println(txtFieldName.getText());
						cleanFields();
					}
					else if(option == JOptionPane.CANCEL_OPTION){
						
					}
				}
			});
			
			gbc.gridx = 1;
			gbc.gridy = 5;
			gbc.gridwidth = 1;
			JButton btnCancel = new JButton("Abbrechen");
			gbl.setConstraints(btnCancel, gbc);
			add(btnCancel);
			btnCancel.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					dispose();
					System.out.println("Button Cancel - klick");
				}
			});
			
			setLocationByPlatform(isCursorSet());
			setVisible(true);
			pack();
		}
			
			
			public void cleanFields(){
				txtFieldName.setText("");
				txtFieldShortDes.setText("");
				txtAreaCopyright.setText("");
				txtAreaIntroText.setText("");
				txtAreaSidebar.setText("");
			}
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}


	
}
