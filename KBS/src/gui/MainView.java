package gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.MenuBar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.regex.Pattern;

import javassist.expr.Instanceof;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import jersey.repackaged.com.google.common.collect.Lists;
import json.JsonFunctions;
import jsonClasses.Community;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import main.Starter;
import controller.Controller;
import restFunctions.RESTBitstreams;
import restFunctions.RESTCollections;
import restFunctions.RESTCommunities;
import restFunctions.RESTItems;
import restFunctions.Server;


public class MainView extends JPanel implements ActionListener, Observer{
	JMenuItem close;
	JMenuItem comSearch;
	JMenuItem collSearch;
	ContentPanel ctnPanel;
	RESTCommunities rcom;
	RESTCollections rcol;
	RESTItems ritem;
	RESTBitstreams rbit;
	JsonFunctions jsonFunctions;
	public JMenuBar mb;
	Model model;
	private String name;

	public MainView(Model model, Server server, String token) {
		rcom = new RESTCommunities();
		rcol = new RESTCollections();
		ritem = new RESTItems();
		rbit = new RESTBitstreams();
		jsonFunctions = new JsonFunctions();
		this.model = model;
		model.addObserver(this);
		ctnPanel = new ContentPanel();
		mb = new JMenuBar();
		final JMenuItem logout = new JMenuItem("Logout");
		close = new JMenuItem("Close");
		comSearch = new JMenuItem("Search");
		final JMenuItem comInsert = new JMenuItem("Insert");
		final JMenuItem comDel = new JMenuItem("Delete");
		comDel.setEnabled(false);
		
		final JMenuItem collSearch = new JMenuItem("Search");
		final JMenuItem collInsert = new JMenuItem("Insert");
		collInsert.setEnabled(false);
		final JMenuItem collDel = new JMenuItem("Delete");
		collDel.setEnabled(false);
		
		final JMenuItem itSearch = new JMenuItem("Search");
		final JMenuItem itInsert = new JMenuItem("Insert");
		itInsert.setEnabled(false);
		final JMenuItem itDel = new JMenuItem("Delete");
		itDel.setEnabled(false);
		
		final JMenuItem bitSearch = new JMenuItem("Search");
		final JMenuItem bitInsert = new JMenuItem("Insert");
		itInsert.setEnabled(false);
		final JMenuItem bitDel = new JMenuItem("Delete");
		bitDel.setEnabled(false);
		

		
		final JMenu community = new JMenu("Community");
		final JMenu collection = new JMenu("Collections");
		final JMenu item = new JMenu("Item");
		final JMenu bitstream = new JMenu("Bitstream");
		final JMenu file = new JMenu("File");
		final JMenu rest = new JMenu("REST Commands");
		final JMenu prioCommand = new JMenu("Priority Command");
		
		final JMenuItem showAllCommunities = new JMenuItem("ShowAllCommunities");
		prioCommand.add(showAllCommunities);
		showAllCommunities.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
//				ObjectMapper mapper = new ObjectMapper();
//				List<T> list = mapper.readValue(jsonString, new TypeReference<List<SomeClass>>() { });
//				SomeClass[] array = mapper.readValue(jsonString, SomeClass[].class);
//				List<Community[]> communities = 
//					     Arrays.asList(jsonFunctions.parseStringArrayIntoJson( rcom.showAllCommunities(server, token).toString(), Community[].class ) );	
				
//List<JsonFunctions> asList = Arrays.asList(jsonFunctions.parseStringArrayIntoJson(rcom.showAllCommunities(server, token), jsonFunctions.getClass() ) );
//			jsonFunctions.parseStringArrayIntoJson(rcom.showAllCommunities(server, token), jsonFunctions.getClass());
				String[] parts = RESTCommunities.showAllCommunities(server, token).split(Pattern.quote(",")); 
				ctnPanel.txtArea.setText(parts[1]);
			}
		});
		final JMenuItem showAllCollections = new JMenuItem("ShowAllCollections");
		prioCommand.add(showAllCollections);
		showAllCollections.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String[] parts = RESTCollections.showAllCollections(server, token).split(Pattern.quote(",")); 
				ctnPanel.txtArea.setText(parts[1]);
			}
		});
		
		final JMenu help = new JMenu("Help");

		file.add(logout);
		file.addSeparator();
		file.add(close);

		close.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e){
			System.out.println("Close klick");

			System.exit(0);
		}});
		community.add(comSearch);
		comSearch.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					System.out.println("Com Search klick");
					ctnPanel.lblBar.lbl.setText("Community Search");
					RESTCommunities.showAllCommunities(server, token);
					new ComSearchView(ctnPanel, server, token);
				}
		});
		community.add(comInsert);
		comInsert.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.out.println("Com Insert klick");
				ctnPanel.lblBar.lbl.setText("Insert Community");
//				rcom.showAllCommunities(server, token);
				new ComInsertView(ctnPanel, server, token);
			}
	});

		community.add(comDel);
		collection.add(collSearch);
		collSearch.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.out.println("Col Search klick");
				ctnPanel.lblBar.lbl.setText("Collection Search");
				rcol.showAllCollections(server, token);
				new ColSearchView(ctnPanel, server, token);
			}
	});
		collection.add(collInsert);
		collection.add(collDel);
		item.add(itSearch);
		itSearch.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.out.println("Item Search klick");
				ctnPanel.lblBar.lbl.setText("Item Search");
				ritem.showAllItems(server, token);
				new ItemSearchView(ctnPanel, server, token);
			}
	});
		item.add(itInsert);
		item.add(itDel);
		
		bitstream.add(bitSearch);
		bitSearch.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.out.println("Bitstream Search klick");
				ctnPanel.lblBar.lbl.setText("Bitstream Search");
				rbit.showAllBitstreams(server, token);
				new BitSearchView(ctnPanel, server, token);
			}
	});
		bitstream.add(bitInsert);
		bitstream.add(bitDel);
		
		
		mb.add(file);
		mb.add(rest);
		rest.add(community);
		rest.add(collection);
		rest.add(item);
		rest.add(bitstream);
		mb.add(prioCommand);
		
		mb.add(help);

			
		setLayout(new BorderLayout());
		add(ctnPanel);
		setVisible(true);
		

	}

	public MainView(
		    @JsonProperty("name") String name) {
		this.name = name;
}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void update(Observable arg0, Object arg1) {
		Model model = (Model) arg0;
		String str = (String) arg1;
		if(arg0 instanceof Model){
		   if(str.equals(Controller.JSONObject)){
//			   ContentPanel.repaint();
		   }
		}
	}
	


}