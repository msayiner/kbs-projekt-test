package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

import restFunctions.RESTCollections;
import restFunctions.Server;

public class ColSearchView  extends JDialog implements ActionListener{
	private final JComboBox box;
	private final String[] search = {"Alle Collections" , "Collections mit ID", "Items aus Collections mit ID"};

	public ColSearchView(ContentPanel ctnPanel, Server server, String token) {
	
	setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
	setTitle("Suchen von Collections");
//	setBounds(400, 300, 300, 200);
	GridBagLayout gbl = new GridBagLayout();
	setLayout(gbl);
			
	GridBagConstraints gbc = new GridBagConstraints();
	gbc.fill=GridBagConstraints.HORIZONTAL;;
	gbc.insets = new Insets(2, 2, 2, 2);
	
	gbc.gridx = 1;
	gbc.gridy = 1;
	gbc.gridwidth = 1;
	JTextField txtField = new JTextField(10);
	gbl.setConstraints(txtField, gbc);
	add(txtField);
	txtField.setEditable(false);
	
	gbc.gridx = 1;
	gbc.gridy = 0;
	gbc.gridwidth = 1;
	box = new JComboBox(search);
	gbl.setConstraints(box, gbc);
	add(box);
	box.addItemListener(new ItemListener(){

		@Override
		public void itemStateChanged(ItemEvent ie) {	
			if(box.getSelectedIndex() == 0){
                txtField.setEditable(false);
                txtField.setText("Disable");
		}else{txtField.setEditable(true);
		txtField.setText("Enabled");
		}
	}});
	
	gbc.gridx = 0;
	gbc.gridy = 0;
	gbc.gridwidth = 1;
	JLabel comboLabel = new JLabel("Suche nach:");
	gbl.setConstraints(comboLabel, gbc);
	add(comboLabel);
	
	gbc.gridx = 0;
	gbc.gridy = 1;
	gbc.gridwidth = 1;
	JLabel idLabel = new JLabel("ID:");
	gbl.setConstraints(idLabel, gbc);
	add(idLabel);
			
	
	gbc.gridx = 0;
	gbc.gridy = 2;
	gbc.gridwidth = 1;
	JButton btnSearch = new JButton("Suchen");
	gbl.setConstraints(btnSearch, gbc);
	add(btnSearch);
	btnSearch.addActionListener(new ActionListener(){
		
		@Override
		public void actionPerformed(ActionEvent eve){
						
			if(box.getSelectedIndex() == 0){
//				String txtFieldValue = txtField.getText();
//				int idValue = Integer.parseInt(txtFieldValue);
				String[] parts = RESTCollections.showAllCollections(server, token).split(Pattern.quote(",")); 
				ctnPanel.txtArea.setText(parts[1]);
			}
			
			
			if(box.getSelectedIndex() == 1){
				String txtFieldValue = txtField.getText();
				int idValue = Integer.parseInt(txtFieldValue);
				String[] parts = RESTCollections.showCollectionWithId(server, token, idValue).split(Pattern.quote(",")); 
				ctnPanel.txtArea.setText(parts[1]);
			}
			
			if(box.getSelectedIndex() == 2){
				String txtFieldValue = txtField.getText();
				int idValue = Integer.parseInt(txtFieldValue);
				String[] parts = RESTCollections.showItemsInCollection(server, token, idValue).split(Pattern.quote(",")); 
				ctnPanel.txtArea.setText(parts[1]);
			}
			
		}
	});
	
	gbc.gridx = 1;
	gbc.gridy = 2;
	gbc.gridwidth = 1;
	JButton btnCancel = new JButton("Abbrechen");
	gbl.setConstraints(btnCancel, gbc);
	add(btnCancel);
	btnCancel.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			dispose();
			System.out.println("Button Cancel - klick");
		}
	});
	
	setLocationRelativeTo(getParent());
	setVisible(true);
	pack();
}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
