package gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;


public class ContentPanel extends JPanel implements ActionListener{
		

	public JButton btn;
	public JTextArea txtArea;
	LabelBar lblBar;
	public ComSearchView csv;

	public ContentPanel() {
		
		setLayout(new BorderLayout());
		System.out.println("im ContentPanel");	
		lblBar = new LabelBar();
		txtArea = new JTextArea();
		JScrollPane scrollPane = new JScrollPane (txtArea, 
	            ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
	            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		btn = new JButton("Test");
		btn.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
			System.out.println("Button klick");
			setText("Teest");
			lblBar.lbl.setText("Teeest");
			System.out.println(txtArea.getText() + "getText");
			
		}});

		add(lblBar, BorderLayout.NORTH);
		add(btn, BorderLayout.SOUTH);
		add(scrollPane);
		this.setPreferredSize(new Dimension(900,500));
	}

//	public void initialized(){
//		
//	
//		txtArea = new JTextArea();
//		btn = new JButton("Test");
//		btn.addActionListener(new MyActionListener(null, this, null, null));
//		btn.setActionCommand("btnTest");
//		add(lblBar, BorderLayout.NORTH);
//		add(btn, BorderLayout.SOUTH);
//		add(txtArea, BorderLayout.CENTER);
//	}
	public String getText(){
		return txtArea.getText();
	}
	
	public void setText(String string){
		this.txtArea.setText(string);
		System.out.println("Setter setText " + string);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}


}