package gui;

import java.util.Observable;

import controller.Controller;

public class Model extends Observable{
	String JSONObejct = new String();

	public String getJSONObejct() {
		return JSONObejct;
	}

	public void setJSONObejct(String jSONObejct) {
		JSONObejct = jSONObejct;
		setChanged();
		notifyObservers(Controller.JSONObject);
	}
}
