package jsonClasses;

import java.util.Arrays;

/**
 * Diese Klasse definiert den Aufbau eines Bitstream-Json-Objektes
 *
 */

public class Bitstream {
	
	public int id;
	public String name;
	public String handle;
	public String type;
	public String link;
	public String[] expand;
	public String bundleName;
	public String description;
	public String format;
	public String mimeType;
	public int sizeBytes;
	public String parentObject;
	public String retrieveLink;
	public Object checkSum;	// was f�r ein Klassentyp?
	public int sequenceId;
	public String policies;
	
	public String toString() {
		return "Bitstream [id=" + id + ", name=" + name + ", handle=" + handle
				+ ", type=" + type + ", link=" + link + ", expand="
				+ Arrays.toString(expand) + ", bundleName=" + bundleName
				+ ", description=" + description + ", format=" + format
				+ ", mimeType=" + mimeType + ", sizeBytes=" + sizeBytes
				+ ", parentObject=" + parentObject + ", retrieveLink="
				+ retrieveLink + ", checkSum=" + checkSum + ", sequenceId="
				+ sequenceId + ", policies=" + policies + "]";
	}
}
