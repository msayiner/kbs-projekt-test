package jsonClasses;

import java.util.Arrays;

import json.JsonFunctions;

/**
 * Diese Klasse definiert den Aufbau eines Community-Json-Objektes
 *
 */

public class Community {

	public int id;
	public String name;
	public String handle;
	public String type;
	public String link;
	public String[] expand;
	public String logo;
	public String parentCommunity;
	public String copyrightText;
	public String introductoryText;
	public String shortDescription;
	public String sidebarText;
	public String countItems;
	public String[] subcommunities;
	public String[] collections;

	public Community(String name, String copyrightText,
			String introductoryText, String shortDescription, String sidebarText) {
		super();
		this.name = name;
		this.copyrightText = copyrightText;
		this.introductoryText = introductoryText;
		this.shortDescription = shortDescription;
		this.sidebarText = sidebarText;
	}

	/**
	 * Erzeugt ein JsonString vom Community Objekt
	 * 
	 * @return JsonString
	 */
	public String toJsonString() {
		return "{\""
				+ "name\":\""+name+"\","
				+ "\"copyrightText\":\""+copyrightText+"\","
				+ "\"introductoryText\":\""+introductoryText+"\","
				+ "\"shortDescription\":\""+shortDescription+"\","
				+ "\"sidebarText\":\""+sidebarText
				+ "\"}";
	}

	/**
	 * Erzeugt ein String vom Community Objekt
	 * 
	 * @return String
	 */
	public String toString() {
		return "Community [id=" + id + ", name=" + name + ", handle=" + handle
				+ ", type=" + type + ", link=" + link + ", expand="
				+ Arrays.toString(expand) + ", logo=" + logo
				+ ", parentCommunity=" + parentCommunity + ", copyrightText="
				+ copyrightText + ", introductoryText=" + introductoryText
				+ ", shortDescription=" + shortDescription + ", sidebarText="
				+ sidebarText + ", countItems=" + countItems
				+ ", subcommunities=" + Arrays.toString(subcommunities)
				+ ", collections=" + Arrays.toString(collections) + "]";
	}
}
