package jsonClasses;

import java.util.Arrays;

/**
 * Diese Klasse definiert den Aufbau eines Item-Json-Objektes
 *
 */

public class Item {

	public int id;
	public String name;
	public String handle;
	public String type;
	public String link;
	public String[] expand;
	public String lastModified;
	public String parentCollection;
	public String[] parentCollectionList;
	public String[] parentCommunityList;
	public String bitstreams;
	public boolean archived;
	public boolean withdrawn;
	
	public String toString() {
		return "Item [id=" + id + ", name=" + name + ", handle=" + handle
				+ ", type=" + type + ", link=" + link + ", expand="
				+ Arrays.toString(expand) + ", lastModified=" + lastModified
				+ ", parentCollection=" + parentCollection
				+ ", parentCollectionList="
				+ Arrays.toString(parentCollectionList)
				+ ", parentCommunityList="
				+ Arrays.toString(parentCommunityList) + ", bitstreams="
				+ bitstreams + ", archived=" + archived + ", withdrawn="
				+ withdrawn + "]";
	}
}
