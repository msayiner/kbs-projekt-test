package jsonClasses;

import java.util.Arrays;

/**
 * Diese Klasse definiert den Aufbau eines Collection-Json-Objektes
 *
 */

public class Collection {

	public int id;
	public String name;
	public String handle;
	public String type;
	public String link;
	public String[] expand;
	public String logo;
	public String parentCommunity;
	public String[] parentCommunityList;
	public String[] items;
	public String license;
	public String copyrightText;
	public String introductoryText;
	public String shortDescription;
	public String sidebarText;
	public int numberItems;
	
	public Collection(int id, String name, String handle, String type,
			String link, String[] expand, String logo, String parentCommunity,
			String[] parentCommunityList, String[] items, String license,
			String copyrightText, String introductoryText,
			String shortDescription, String sidebarText, int numberItems) {
		super();
		this.id = id;
		this.name = name;
		this.handle = handle;
		this.type = type;
		this.link = link;
		this.expand = expand;
		this.logo = logo;
		this.parentCommunity = parentCommunity;
		this.parentCommunityList = parentCommunityList;
		this.items = items;
		this.license = license;
		this.copyrightText = copyrightText;
		this.introductoryText = introductoryText;
		this.shortDescription = shortDescription;
		this.sidebarText = sidebarText;
		this.numberItems = numberItems;
	}

	public String toString() {
		return "Collection [id=" + id + ", name=" + name + ", handle=" + handle
				+ ", type=" + type + ", link=" + link + ", expand="
				+ Arrays.toString(expand) + ", logo=" + logo
				+ ", parentCommunity=" + parentCommunity
				+ ", parentCommunityList="
				+ Arrays.toString(parentCommunityList) + ", items="
				+ Arrays.toString(items) + ", license=" + license
				+ ", copyrightText=" + copyrightText + ", introductoryText="
				+ introductoryText + ", shortDescription=" + shortDescription
				+ ", sidebarText=" + sidebarText + ", numberItems="
				+ numberItems + "]";
	}
}
