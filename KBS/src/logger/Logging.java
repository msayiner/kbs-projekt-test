package logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Hilfsklasse zum Loggen
 *
 */

public class Logging {

	/** Pfad zur Logdatei */
	private static String logPath;
	static{
		logPath="logs/Log.txt";
	}
	
	/** Logging eines Strings in Datei und Ausgabe auf Konsole
	 * 
	 * @param s Logging String
	 */
	
	public static void log(String s){
		s+="\n";
		logC(s);
		logF(s);
	}
	
	/** Logging eines Strings auf Konsole
	 * 
	 * @param s Loggingstring
	 */
	public static void logC(String s){
		System.out.print(s);
	}
	
	/** Logging eines Strings in Datei 
	 * 
	 * @param s Loggingstring
	 */
	public static void logF(String s){
		Date date = java.util.Calendar.getInstance().getTime();
		SimpleDateFormat dateFormatter = 
		 new SimpleDateFormat("yyyy.MM.dd HH.mm.ss: ");
		String dateString = dateFormatter.format(date);
		try {
			BufferedWriter bW = new BufferedWriter(new FileWriter(logPath,true));
			bW.write(dateString+s);
			bW.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
