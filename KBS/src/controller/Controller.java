package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import gui.ComSearchView;
import gui.ContentPanel;
import gui.LabelBar;
import gui.MainView;
//import gui.MyActionListener;


import gui.Model;
import gui.TargetFrame;

import javax.swing.JFrame;
import javax.swing.text.View;

import main.Starter;
import restFunctions.RESTCommunities;
import restFunctions.Server;

public class Controller {

	 ContentPanel ctnPanel;
	 LabelBar lblBar;
	 ComSearchView csv;
	 RESTCommunities rcom;
	 MainView mv;
	 public final static String JSONObject = "";
	 
	public Controller(Server server, String token){
//		Login lg = new Login(mv, server, token);
		Model model = new Model();
		TargetFrame tf = new TargetFrame();
		MainView mv = new MainView(model, server, token);
		tf.setContentPane(mv);
		tf.setJMenuBar(mv.mb);
		tf.pack();
		tf.setLocationRelativeTo(null);
		tf.setVisible(true);

	}
	


}