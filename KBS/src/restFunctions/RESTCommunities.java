package restFunctions;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import jsonClasses.Community;
import logger.Logging;

/**
 * 
 * REST-Methoden zum Zugriff auf Communities
 *
 */

public class RESTCommunities {

	/**
	 * Zeige alle Communities
	 * 
	 * @param server Server
	 * @param token Token
	 * @return Json-String
	 */
	
	public static String showAllCommunities(Server server, String token) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("communities");

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige alle Communities: "+returnString);
		
		return returnString;
	}
	
	/**
	 * Zeige alle Top-Communities
	 *  
	 * @param server Server
	 * @param token Token
	 * @return Json-String
	 */
	
	public static String showAllTopCommunities(Server server, String token) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("communities/top-communities");

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige alle Top-Level-Communities: "+returnString);
		
		return returnString;
	}
	
	/**
	 * Zeige Community mit ID
	 * 
	 * @param server Server
	 * @param token Token
 	 * @param id ID der Community
	 * @return Json-String
	 */
	
	public static String showCommunityWithId(Server server, String token, int id) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("communities/"+id);

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige Community mit der ID: "+id+": "+returnString);
		
		return returnString;
	}
	
	/**
	 * Zeige Collection einer Community mit ID
	 * 
	 * @param server Server
	 * @param token Token
	 * @param id ID der Community
	 * @return Json-String
	 */
	
	public static String showCollectionInCommunity(Server server, String token, int id) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("communities/"+id+"/collections");

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige Collections aus der Community mit der ID: "+id+": "+returnString);
		
		return returnString;
	}
	
	/**
	 * Zeige alle Subcommunities einer Communitiy mit ID
	 * 
	 * @param server Server
	 * @param token Token
	 * @param id Community ID
	 * @return Json-String 
	 */
	
	public static String showSubcommunitiesFromCommunityWithId(Server server, String token, int id) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("communities/"+id+"/communities");

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige alle Subcommunities aus der Community mit der ID: "+id+": "+returnString);
		
		return returnString;
	}
	
	/**
	 * Erstelle eine Top-level-Community
	 * 
	 * @param server Server
	 * @param token Token
	 * @param name Community Name
	 * @param copyrightText Copyright Text
	 * @param introductoryText Introductory Text
	 * @param shortDescription Short Description
	 * @param sidebarText Sidebar Text
	 */
	public static void createTopLevelCommunity(Server server, String token, String name, String copyrightText, String introductoryText, String shortDescription, String sidebarText){
		
		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("/communities");
		
		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Community community = new Community(name, copyrightText, introductoryText, shortDescription, sidebarText);
		
		Response response = invocationBuilder.post(
				Entity.entity(community.toJsonString(),
						MediaType.APPLICATION_JSON));
		
		response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			token = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		Logging.log("Top-Level-Community von folgenden Variablen erstellt:"+community);
	}
	
}
