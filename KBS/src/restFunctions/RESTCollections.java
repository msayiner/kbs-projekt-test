package restFunctions;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import logger.Logging;

/**
 * 
 * REST Methoden, um auf Collections zuzugreifen.
 *
 */

public class RESTCollections {

	/**
	 * Zeige alle Collections
	 * 
	 * @param server Server
	 * @param token Token
	 * @return Json-String
	 */
	
	public static String showAllCollections(Server server, String token) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("collections");

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige alle Collections: "+returnString);
		
		return returnString;
	}
	
	/**
	 * Zeige Collection mit ID
	 * 
	 * @param server Server
	 * @param token Token
	 * @param id ID der Collection
	 * @return Json-String
	 */
	
	public static String showCollectionWithId(Server server, String token, int id) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("collections/"+id);

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige Collection mit der ID: "+id+": "+returnString);
		
		return returnString;
	}
	
	/**
	 * Zeige alle Items in Collection
	 * 
	 * @param server Server
	 * @param token Token
	 * @param id ID der Collection
	 * @return Json-String
	 */
	
	public static String showItemsInCollection(Server server, String token, int id) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("collections/"+id+"/items");

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige Items aus der Collection mit der ID: "+id+": "+returnString);
		
		return returnString;
	}
	
}
