package restFunctions;

/**
 * 
 * Klasse fuer die Serverdaten 
 */

public class Server {
	
	String ip;
	int port;
	
	/**
	 * @param ip IP-Adresse 
	 * @param port Port
	 */
	
	public Server(String ip, int port){
		this.ip = ip;
		this.port = port;
	}
	
	/**
	 * Hole die IP
	 * @return IP-Adresse
	 */
	public String getIp() {
		return ip;
	}
	
	/**
	 * Hole den Port
	 * @return Port
	 */
	public int getPort() {
		return port;
	}
}
