package restFunctions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.swing.JFileChooser;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import json.JsonFunctions;
import jsonClasses.Bitstream;
import logger.Logging;

/**
 * 
 * Klassen um auf Bitstreams zugreifen zu k�nnen
 *
 */

public class RESTBitstreams {

	/**
	 * Zeige alle Bitstreams auf Server
	 * @param server Server
	 * @param token Token
	 * @return Json-String
	 */
	
	public static String showAllBitstreams(Server server, String token) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("bitstreams");

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();

		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige alle Bitstreams: "+returnString);
		
		return returnString;
	}
	
	/**
	 * Zeige einen bestimmten Bitstream mit einer ID
	 * 
	 * @param server Server
	 * @param token Token
	 * @param id ID des Bitstreams
	 * @return Json-String
	 */
	
	public static String showBitstreamWithId(Server server, String token, int id) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("bitstreams/"+id);

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige Bitstream mit der ID: "+id+": "+returnString);
		
		return returnString;
	}
	
	/**
	 * Zeige Policies eines Bitstreams mit ID 
	 * 
	 * @param server Server
	 * @param token Token
	 * @param id ID des Bitstreams
	 * @return Json-String
	 */
	
	public static String showPolicyFromBitstreamWithId(Server server, String token, int id) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("bitstreams/"+id+"/policy");
		
		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige Policy vom Bitstream mit der ID: "+id+": "+returnString);
		
		return returnString;
	}
	
	/**
	 * Lade den Inhalt eines Bitstreams mit ID herunter. 
	 * Speicherort wird per Filechooser ausgew�hlt.
	 * 
	 * @param server Server
	 * @param token Token
	 * @param id ID des Bitstreams
	 * 
	 */
	public static void getContentFromBitstreamWithId(Server server, String token, int id) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("bitstreams/"+id+"/retrieve");

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		byte[] returnByte = response.readEntity(byte[].class);

		// Hole den Dateinamen mit Endung des Bitstreams
		String name = JsonFunctions.parseStringIntoJson(showBitstreamWithId(server, token, id), Bitstream.class).name;
		
		if(response.getStatus() != 200){
			returnByte = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Erzeuge einen FileChooser, mit dessen Hilfe der Bitstream am gew�nschtem Ort abgespeichert wird
		try {
			JFileChooser jfc = new JFileChooser();
			jfc.setSelectedFile(new File(name));
			jfc.showOpenDialog(null);
			OutputStream o = new FileOutputStream(jfc.getSelectedFile());
			o.write(returnByte);
			o.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	
}
