package restFunctions;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import logger.Logging;



/**
 * Anmeldung an Server um Session Token zu erhalten
 *
 */

public class RESTAdmin {

	/**
	 * Login
	 * 
	 * @param server Server 
	 * @param username Username
	 * @param password Passwwort
	 * @return Token
	 */
	
	public static String login(Server server, String username, String password) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("login");

		Response postResponse = resourceWebTarget.request(
				MediaType.APPLICATION_JSON).post(
				Entity.entity("{\"email\":\"" + username
						+ "\", \"password\":\"" + password + "\"}",
						MediaType.APPLICATION_JSON));
		
		String token = postResponse.readEntity(String.class);
		
		if(postResponse.getStatus() != 200){
			token = null;
			Logging.log("Fehlercode: "+postResponse.getStatus());
		}
		
		// Log
		Logging.log("Login: Username: "+username+" Token: "+token);
		
		return token;
	}
	
	/**
	 * Logout
	 * 
	 * @param server Server
	 * @param token Token
	 * @return Hat Abmeldung funktioniert?
	 */
	public static boolean logout(Server server, String token){
		
		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("logout");

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.TEXT_PLAIN_TYPE);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.post(null);
		
		String returnString = response.readEntity(String.class);
		
		// Log
		Logging.log("Logout: "+returnString);
		
		if(returnString.equals(""))
			return true;
		return false;
	}

}
