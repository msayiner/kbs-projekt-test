package restFunctions;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import logger.Logging;

/**
 * 
 * REST Methoden f�r den Zugriff auf Items
 *
 */

public class RESTItems {
	
	/**
	 * Zeige alle Items 
	 * 
	 * @param server Server
	 * @param token Token
	 * @return Json-String 
	 */
	
	public static String showAllItems(Server server, String token) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("items");

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige alle Items: "+returnString);
		
		return returnString;
	}
	
	/**
	 * Zeige Item mit ID
	 * 
	 * @param server Server
	 * @param token Token
	 * @param id ID des Items
	 * @return Json-String
	 */
	
	public static String showItemWithId(Server server, String token, int id) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("items/"+id);

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige Item mit der ID: "+id+": "+returnString);
		
		return returnString;
	}
	
	/**
	 * Zeige Metadaten eines Items mit ID
	 * 
	 * @param server Server
	 * @param token Token
	 * @param id ID 
	 * @return Json-String
	 */
	
	public static String showMetadataFromItemWithId(Server server, String token, int id) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("items/"+id+"/metadata");

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige Metadaten vom Item mit der ID: "+id+": "+returnString);
		
		return returnString;
	}
	
	/**
	 * Zeige alles Bitstreams eines Items mit ID
	 * 
	 * @param server Server
	 * @param token Token
	 * @param id ID des Items
	 * @return Json-String
	 */
	
	public static String showBitstreamFromItemWithId(Server server, String token, int id) {

		Client client = ClientBuilder.newClient();

		WebTarget webTarget = client.target("http://"+server.getIp()+":"+server.getPort()+"/rest");
		WebTarget resourceWebTarget = webTarget.path("items/"+id+"/bitstreams");

		Invocation.Builder invocationBuilder =
		        resourceWebTarget.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("rest-dspace-token", token);
		
		Response response = invocationBuilder.get();
		
		String returnString = response.readEntity(String.class);
		
		if(response.getStatus() != 200){
			returnString = null;
			Logging.log("Fehlercode: "+response.getStatus());
		}
		
		// Log
		Logging.log("Zeige Bitstreams vom Item mit der ID: "+id+": "+returnString);
		
		return returnString;
	}
	
}
