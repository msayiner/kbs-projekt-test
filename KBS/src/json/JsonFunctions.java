package json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * In dieser Klasse gibt es Methoden zur Konvertierung: Json-Objekten <-> Strings 
 *
 */

public class JsonFunctions {

	public static ObjectMapper mapper = new ObjectMapper();
	
	public JsonFunctions(){
		
	}
	
	/** Konvertierung: Json-String in Array einer Antwortklasse
	 * 
	 * @param jsonString Json String
	 * @param c Typ der Antwortklasse
	 */
	
	public static <T> T[] parseStringArrayIntoJson(String jsonString, Class<T> c){
		try {
			mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			@SuppressWarnings("unchecked")
			T[] classType = (T[]) mapper.readValue(jsonString, c);
			for(T o: classType){
				System.out.println(o);
			}
			return classType;
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    return null;
	}
	
	/** 
	 * Konvertierung: Json-String in eine Antwortklasse
	 * 
	 * @param jsonString Json String
	 * @param c Typ der Antwortklasse
	 */
	
	public static <T> T parseStringIntoJson(String jsonString, Class<T> c){
		try {
			T classType = mapper.readValue(jsonString, c);
			System.out.println(classType);
			return classType;
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    return null;
	}
	
}
